import numpy as np
from pathlib import Path
from pdf2image import convert_from_path
import torch
from grid import target_anchors


def pdf_2_images(pdf_file: Path):
    """ Used to generate sample images for generating sample annotations """
    file = pdf_file.parent / pdf_file.stem
    pages = convert_from_path(pdf_file)
    for i, pg in enumerate(pages):
        filename = str(file) + '-' + str(i) + '.png'
        pg.save(filename, 'PNG')


def dummy_data():
    img_shape = (3, 800, 800)
    batch_size = None
    num_gt = 2
    down_sampling_scale = 16
    final_channels = 4  # [P, x, y, h]

    input_img = np.random.rand(*img_shape)
    gts = np.random.randint(0, img_shape[-1], (num_gt, final_channels))

    return dict(img_shape=img_shape, batch_size=batch_size, num_gt=num_gt,
                img=input_img, gts=gts,
                down_sample_scale=down_sampling_scale,
                final_channels=final_channels)


class DummyDataset(torch.utils.data.Dataset):
    """This dataset class is created to understand pytorch-lightning working
     and saving logs, models, etc. """

    def __init__(self):
        self.data = torch.randn((100, 3, 800, 800), dtype=torch.float32)
        self.gt = torch.randint(0, 800, (100, 2, 4), dtype=torch.float32)
        self.shape = (800, 800)
        self.down_sample_scale = 16
        self.final_channels = 4

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        gt = self.gt[index]
        grid = target_anchors({'img_shape': self.shape, 'down_sample_scale': self.down_sample_scale,
                               'final_channels': self.final_channels, 'gts': gt})
        return {'img': self.data[index],
                'gt': grid.float()}


if __name__ == "__main__":
    dataset = DummyDataset()
    for set in dataset:
        print()