import pytorch_lightning as pl
import torch
from abc import ABC
from torchvision.utils import make_grid


class BackLogs(pl.LightningModule, ABC):
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.module_names = None
        self.tags = dict()

    def log_parameters(self):
        for name, params in self.named_parameters():
            self.logger.experiment.add_histogram(name, params,
                                                 self.current_epoch)

    def log_images(self, img_dic, postfix='train', step=None, scale_factor=None):
        step = step if step is not None else self.current_epoch
        for name, img in img_dic.items():
            x_input = img.get('x_input', torch.Tensor([])).detach()
            x_featMap = img.get('x_featMap', torch.Tensor([])).detach()
            y_grid = img.get('y_grid', torch.Tensor([])).detach()

            tag = name + "_" + postfix
            if len(x_input.shape) > 3:
                ims_pr_row = max(8, x_input.shape[-4])
                """If you want upscale an image use the below code:
                import torch.nn.functional as F
                channels_scaled = F.interpolate(
                    input=channels,
                    scale_factor=scale_factor,
                    mode="bilinear", # changed because channels dictionary size increased from 3 to 5
                    align_corners=False,  # commented because we can't use align_corners with mode nearest
                ).clamp(0,1)
                """
                im_grid = make_grid(tensor=x_input, nrow=ims_pr_row)
                self.logger.experiment.add_image(tag, im_grid, step)

    def log_scalar(self, tag, value, step=None, postfix='train'):
        self.logger.experiment.add_scalar(tag, value, step)


