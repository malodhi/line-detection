import torch
from PIL import Image
from pathlib import Path
from torchvision import transforms
import xml.etree.ElementTree as ET
from grid import target_anchors


class Dataset(torch.utils.data.Dataset):
    def __init__(self, annotations_dir: Path = None):
        self.annotations_dir = annotations_dir
        self.resize_img = (800, 800)
        self.im_mode = 'RGB'
        self.transform = None
        self.down_sample_scale = 16
        self.final_channels = 4

        self.data = self.format_gt(self.read_xml(annotations_dir), self.resize_img)
        self.img_files = list(self.data.keys())

    def __len__(self):
        """
        :return: number of usable image which have any ground truth of line in them.
        """
        return len(self.img_files)

    def __getitem__(self, index):
        filename = self.img_files[index]
        filepath = self.annotations_dir / 'images' / filename

        img = Image.open(filepath).convert(self.im_mode).resize(self.resize_img)
        gt = torch.tensor(self.data[filename])
        grid = target_anchors({'img_shape': self.resize_img, 'down_sample_scale': self.down_sample_scale,
                               'final_channels': self.final_channels, 'gts': gt})

        if self.transform is None:
            self.transform = transforms.ToTensor()

        img_trfm = self.transform(img)

        return {'img': img_trfm, 'gt': grid.float()}

    @staticmethod
    def read_xml(file: Path) -> dict:
        """
        :param file:
        :return: {'filename':
                    {
                    'width' : float(), 'height' : float(), 'polylines': [[x0,y0,x1,y1], [x0,y0,x1,y1], [x0,y0,x1,y1]]
                    }
                }
        """
        xml_file = file / 'annotations.xml'
        doc_tree = ET.parse(xml_file.as_posix())
        doc_root = doc_tree.getroot()
        data = dict()
        for first_order in doc_root:
            if first_order.tag == 'image':
                # single image:
                data[first_order.attrib['name']] = dict()
                data[first_order.attrib['name']]['width'] = int(first_order.attrib['width'])
                data[first_order.attrib['name']]['height'] = int(first_order.attrib['height'])
                data[first_order.attrib['name']]['polylines'] = list()
                for second_order in first_order:
                    # single image annotations:
                    # todo: the 'label' is to be consistent with every annotator.
                    if second_order.tag == 'polyline' and second_order.attrib['label'] == 'text':
                        polyline = second_order.attrib['points']
                        points = list()
                        for coordinates in polyline.split(';'):
                            x, y = coordinates.split(',')
                            points.extend([float(x), float(y)])
                        data[first_order.attrib['name']]['polylines'].append(points)
                # delete image if it doesn't have any polylines:
                if len(data[first_order.attrib['name']]['polylines']) == 0:
                    del data[first_order.attrib['name']]

        return data

    @staticmethod
    def format_gt(parsed_data: dict, resize: tuple) -> dict:
        """
        :param parsed_data:
                {'filename':
                    {
                    'width' : float(), 'height' : float(), 'polylines': [[x0,y0,x1,y1], [x0,y0,x1,y1], [x0,y0,x1,y1]]
                    }
                }
        :param resize:
        :return:
                {'filename': [ [1, x0, y0, height], [1, x0, y0, height], [1, x0, y0, height] ]  }
        """
        data = dict()
        resize_width, resize_height = resize
        for filename, attr in parsed_data.items():
            data[filename] = list()
            width = attr['width']
            height = attr['height']
            polylines = attr['polylines']
            for line in polylines:
                x0, x1, y0, y1 = line
                # todo: recheck the below logic to check height ....
                # todo: right now we assume the polyline is sketch bottom-up
                gt_height = y0 - y1
                if gt_height <= 0:
                    continue
                x0 = (x0 / width) * resize_width
                y0 = (y0 / height) * resize_height
                gt_height = (gt_height / height) * resize_height
                data[filename].append([1.0, x0, y0, gt_height])
            if len(data[filename]) == 0:
                del data[filename]

        return data


class InferenceDataset(torch.utils.data.Dataset):
    def __init__(self, dir: Path):
        self.dir = dir
        self.resize_img = (800, 800)
        self.im_mode = 'RGB'
        self.transform = None
        self.down_sample_scale = 16
        self.final_channels = 4
        self.img_files = list()

        self.read_images(dir)

    def __len__(self):
        return len(self.img_files)

    def __getitem__(self, index):
        file = self.img_files[index]
        img = Image.open(file).convert(self.im_mode).resize(self.resize_img)

        if self.transform is None:
            self.transform = transforms.ToTensor()

        img_trfm = self.transform(img)

        return {'img': img_trfm, 'filename': str(file)}

    def read_images(self, files_path, ends=['.png']):
        if not files_path.is_absolute():
            files_path = files_path.absolute()
        if files_path.is_file() and files_path.suffix in ends:
            self.img_files.append(files_path)
        if files_path.is_dir():
            for file in files_path.iterdir():
                self.read_images(file)


if __name__ == '__main__':
    ds = InferenceDataset(Path('line_detection_dataset/test_imgs'))
    for img_dict in ds:
        print(img_dict['img'].shape)