from abc import ABC
from pathlib import Path
import torch
import torch.nn as nn
from typing import Optional
from dataloader import Dataset
import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks import EarlyStopping, ModelCheckpoint
from torch.utils.data import DataLoader, random_split
from encoder import Backbone
from utils import DummyDataset
from typing import List, Any
from logger import BackLogs


"""
-----
Professionally we should implement two different class, one for dataloaders (using pl.LightningDataModule) and the 
other for train_steps and model (using pl.LightningModule). This would make it easy for us to test the model on 
various different datasets by just changing that particular dataset class.
Because in this case we are going to test our model on only one dataset thus we can incorporate pl.LightningDataModule 
functions in pl.LightningModule class.

Callbacks & Hooks: 
-----------------
Callback is a class (for e.g LightningModule) and it has function called Hooks (for e.g train_dataloader)
"""

#todo:
"""  Below is test to debug val_loss not found error:
class MyEarlyStopping(EarlyStopping):
    def on_validation_end(self, trainer, pl_module):
        # override this to disable early stopping at the end of val loop
        print("validation->", trainer.callback_metrics)

    def on_train_end(self, trainer, pl_module):
        print("training->", trainer.callback_metrics)
        # instead, do it at the end of training loop
        self._run_early_stopping_check(trainer, pl_module)
"""





class Module(BackLogs, ABC):
    def __init__(self, data_dir: Path=None, valid_split=0.5, batch_size=4):
        super(Module, self).__init__()
        self.data_dir = data_dir
        self.batch_size = batch_size
        self.valid_split = valid_split
        self.backbone = Backbone()
        self.train_set, self.val_set = None, None

    def forward(self, data: torch.Tensor):
        return self.backbone(data)

    def setup(self, stage: Optional[str]):
        if False:
            dataset = DummyDataset()
            self.train_set, self.val_set = random_split(dataset, [70, 30])
        else:
            dataset = Dataset(self.data_dir)
            dataset_size = len(dataset)
            valid_size = int(dataset_size * self.valid_split)
            train_size = dataset_size - valid_size
            self.train_set, self.val_set = random_split(dataset, [train_size, valid_size])

    def train_dataloader(self) -> DataLoader:
        return DataLoader(self.train_set, self.batch_size, True)

    def training_step(self, batch, batch_index):
        """
        If we don't implement forward function in this class we use:
        featMap = self.backbone(batch['img'])
        Otherwise if we have forward function we can use the below line:
        """
        featMap = self(batch['img'])
        loss = nn.MSELoss()(featMap, batch['gt'])
        self.log('Loss/train', loss, True)
        self.log_scalar('Loss/train', loss, self.global_step)
        self.log_images({
            'x_input': dict(x_input=batch["img"]),
            'x_featMap': dict(x_featMap=featMap),
            'y_grid': dict(y_grid=batch['gt'])}, postfix='train', step=self.global_step, scale_factor=16)
        """  
        1. return loss  ==  return {'loss': loss}  
        2. 'progress_bar' is a built in keyword which if sent would be accepted by pl and we can return 
            accuracy dict against this dictionary. 
        """
        return {'loss': loss}

    def val_dataloader(self) -> DataLoader:
        return DataLoader(self.val_set, self.batch_size, True)

    def validation_step(self, batch, batch_index):
        featMap = self(batch['img'])
        loss = nn.MSELoss()(featMap, batch['gt'])
        self.log('Loss/val', loss, True)
        self.log_scalar('Loss/val', loss, self.global_step)
        # self.logger.experiment.add_image('Input/x_val', featMap, self.global_step)
        # self.logger.experiment.add_image('Input/y_val', batch['gt'], self.global_step)
        self.log_images({
            'x_input': dict(x_input=batch["img"]),
            'x_featMap': dict(x_featMap=featMap),
            'y_grid': dict(y_grid=batch['gt'])},  postfix='valid', step=self.global_step, scale_factor=16)
        return {'val_loss': loss}

    """ An example to apply different functionalities at the end of all validation steps in an epoch:
    def validation_epoch_end(self, val_step_outputs: List[Any]) -> dict:
        # Don't confuse x['loss'] with training loss. Its only & specifically val loss.
        avg_val_loss = torch.Tensor([x['loss'] for x in val_step_outputs]).mean()
        return {'val_loss': avg_val_loss}
    """

    def configure_optimizers(self):
        return torch.optim.Adam(params=self.parameters(), lr=0.0001)


if __name__ == '__main__':
    """
    pl.Trainer args:
    num_nodes -> number of machines with different gpus. if the nodes are on same filesystem the data will be 
                downloaded once otherwise we would have to use the function 'prepare_data' in pl.lightningModule.
                we don't do the downloading in __init__ function because dataloader are not called unless needed,
                technically pl has Lazy-Loading. basically we create a copy of model on every different machine/node.
    deterministic -> this helps in reproducing the results (like seed).
    callbacks -> this is giving error, not return val_loss, thus we are ignoring it right now 
    """
    logger = TensorBoardLogger("logs", log_graph=True, prefix='training')
    trainer = pl.Trainer(deterministic=True, gpus=0, max_epochs=20, logger=logger,)
                         #  todo: callbacks=[MyEarlyStopping(monitor='val_loss', patience=3, mode='min')])

    module = Module(Path('./line_detection_dataset/').absolute())

    """trainer.fit skips the train_dataloader & valid_dataloader args if module/model has train_dataloader 
        function implemented."""

    trainer.fit(module)

    """Old version to perform pipeline test: 
    model = encoder()
    data = dummy_data()
    anchors_boxes = target_anchors(data)
    predictions = feature_maps(data, model)
    loss = custom_mse(anchors_boxes, predictions)
    print(loss)
    train, val = random_split(dataset, [1, 1])
    dataset = Dataset(Path('./line_detection_dataset/').absolute())
    """
