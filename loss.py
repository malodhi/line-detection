import torch.nn as nn


def custom_mse(anchor, prediction):
    if len(anchor.shape) == 4:
        anchor = anchor.permute(0, 2, 3, 1)
        prediction = prediction.permute(0, 2, 3, 1)
        bsize, width, height, channels = anchor.shape
    elif len(anchor.shape) == 3:
        anchor = anchor.permute(1, 2, 0)
        prediction = prediction.permute(1, 2, 0)
        width, height, channels = anchor.shape
    else:
        raise Exception("Input anchors shape incorrect.")
    mse_error = 0
    for width_step in range(width):
        for height_step in range(height):
            if anchor[width_step, height_step, 0] == 0:
                mse_error += (prediction[width_step, height_step, 0]) ** 2
            else:
                p_error = (anchor[width_step, height_step, 0] - prediction[width_step, height_step, 0]) ** 2
                x_error = (anchor[width_step, height_step, 1] - prediction[width_step, height_step, 1]) ** 2
                y_error = (anchor[width_step, height_step, 2] - prediction[width_step, height_step, 2]) ** 2
                h_error = (anchor[width_step, height_step, 3] - prediction[width_step, height_step, 3]) ** 2
                mse_error += (p_error + x_error + y_error + h_error) / 4
            """
            below is part of natural nn.MSELoss
            for channel in range(channels):
                mse_error += (anchor[width_step, height_step, channel] - \
                prediction[width_step, height_step, channel]) ** 2
    below is part of natural nn.MSELoss
    mse_error = mse_error / (width * height * channels)
    """
    mse_error = mse_error / (width * height)
    return mse_error


def mse(anchor, prediction):
    return nn.MSELoss(anchor, prediction)
