import numpy as np
import torch


def target_anchors(data_collection):
    """
    :param data_collection:
    :return: grid: [final_channel]
    """
    image_shape = data_collection['img_shape']  # [3, 800, 800]
    down_sample_scale = data_collection['down_sample_scale']  # 16
    final_channels = data_collection['final_channels']  # 4
    gts = data_collection['gts']

    grid_height = int(image_shape[-1] / down_sample_scale)  # 800/16 = 50
    grid_width = int(image_shape[-2] / down_sample_scale)  # 800/16 = 50
    grid = np.zeros((final_channels, grid_width, grid_height))  # [4, 50, 50]

    for x_ind, x in enumerate(np.arange(0, image_shape[-2], down_sample_scale)):
        # x_ind -> 0 - 50
        # x -> 0: 16: 800
        for y_ind, y in enumerate(np.arange(0, image_shape[-1], down_sample_scale)):
            # y_ind -> 0 - 50
            # y -> 0: 16: 800

            gt_found = False
            for gt in gts:
                if x <= gt[1] < x + down_sample_scale and y <= gt[2] < y + down_sample_scale:
                    grid[0, x_ind, y_ind] = 1  # probability that gt exist
                    grid[1, x_ind, y_ind] = (gt[1] - x) / grid_width  # x coordinate
                    grid[2, x_ind, y_ind] = (gt[2] - y) / grid_width  # y coordinate
                    grid[3, x_ind, y_ind] = gt[3] / grid_height  # height
                    gt_found = True
                else:
                    grid[0, x_ind, y_ind] = 0  # probability that gt exist
                    grid[1, x_ind, y_ind] = None  # x coordinate
                    grid[2, x_ind, y_ind] = None  # y coordinate
                    grid[3, x_ind, y_ind] = None  # height
                if gt_found:
                    break
    # grid = torch.zeros_like(torch.from_numpy(grid)).cuda()
    grid = torch.zeros_like(torch.from_numpy(grid))
    return grid


def featMap_gt_loc(mappings: torch.Tensor, gts: torch.Tensor) -> list:
    """
    :param mappings:  [batch_size, channels, width, height]
    :param gts:  [batch_size, channels, width, height]
    :return: [batch_size, num_gts, 3] -> 3: x,y,h
    """
