import datetime
from pathlib import Path
import numpy as np
from dataloader import InferenceDataset
from torch.utils.data import DataLoader
import cv2
from main import Module
import json


def infer(files: str = '/home/mansoor/Projects/Line_Detection/line_detection_dataset/test_imgs',
          model_pth: Path = '/home/mansoor/Projects/Line_Detection/logs/default/version_0/checkpoints/epoch=19-step=19.ckpt',
          threshold=0, batch_size=1):

    files = Path(files).absolute()
    model_pth = Path(model_pth).absolute()

    if not model_pth.exists():
        raise Exception("Model checkpoint not exists.")

    dataset = InferenceDataset(files)
    dataloader = DataLoader(dataset, batch_size=batch_size)

    model = Module()
    model.load_from_checkpoint(model_pth)
    model.eval()
    model.freeze()
    results_storage = dict()

    for data in dataloader:
        imgs = data['img']
        filename = data['filename']
        mappings = model(imgs)
        for i in range(batch_size):
            _, mapped_height, mapped_width = mappings[i].shape
            _, input_height, input_width = imgs[i].shape
            scaled_height = int(input_height / mapped_height)
            scaled_width = int(input_width / mapped_width)
            coordinates = []
            for x_ind, x in enumerate(np.arange(0, input_height, scaled_height)):
                # x_ind -> 0 - 50
                # x -> 0: 16: 800
                for y_ind, y in enumerate(np.arange(0, input_width, scaled_width)):
                    # y_ind -> 0 - 50
                    # y -> 0: 16: 800
                    if mappings[i, 0, x_ind, y_ind] <= threshold:
                        continue
                    x = mappings[i, 1, x_ind, y_ind] * scaled_width + x
                    y = mappings[i, 2, x_ind, y_ind] * scaled_height + y
                    h = mappings[i, 3, x_ind, y_ind] * scaled_height
                    coordinates.append((int(x), int(y)))
            results_storage[Path(filename[i]).name] = coordinates
            """
            filename = Path(filename[0])
            save_filename = filename.parent / (filename.stem + '_detected_lines.png')
            save_image(imgs, save_filename)
            if coordinates:
                myImg = cv2.imread(save_filename.as_posix())
                for coord in coordinates:
                    cv2.circle(myImg, coord, radius=3, color=(0, 0, 255), thickness=1)
                cv2.imwrite(save_filename.as_posix(), myImg)
            break
            """
    write_json_predictions(results_storage, 'line_detection_dataset/test_pred/')


def write_json_predictions(data: dict, file: str) -> None:
    file = file + ' ' + str(datetime.datetime.now()) + '.json'
    if Path(file).exists():
        raise Exception("File to write already exists.")
    with open(file, 'a') as fp:
        json.dump(data, fp, indent=4)


def plot_results(test_img_dir: str, test_pred_file: str, test_plots_dir: str):
    if not Path(test_pred_file).exists():
        raise Exception('Test Predictions Files Not Found!')

    with open(test_pred_file) as fp:
        data = json.load(fp)

    for file in data.keys():
        filename = test_img_dir + '/' + file
        if not Path(filename).exists():
            print(f"{file} not found in {test_img_dir}")
            continue
        coordinates = data[file]
        save_img = test_plots_dir + '/' + file
        myImg = cv2.imread(filename)
        for coord in coordinates:
            cv2.circle(myImg, tuple(coord), radius=3, color=(0, 0, 255), thickness=1)
        cv2.imwrite(save_img, myImg)


if __name__ == "__main__":
    plot_results('line_detection_dataset/test_imgs',
                 'line_detection_dataset/test_pred/2021-09-13 14:40:43.791499.json',
                 'line_detection_dataset/test_plots')
