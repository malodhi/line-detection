import numpy as np
import torch
from torch import nn
import torchvision.models as models


def Backbone(name: str = 'vgg16'):
    base_encoder = getattr(models, name)(pretrained=False)
    base_encoder = base_encoder.features[0:-1]  # output: [512, 50, 50]
    """
    Now lets add an addition regression layer which outputs 4 channels:
    Channel 2: x center coordinate
    Channel 3: y center coordinate
    Channel 4: height
    Output height/width of image in feature map will be same as one produced by
    base_encoder, 50*50, because we didn't add any pooling layer and kept 
    stride 1 and pooling also 1.  
    """
    reg_layer = nn.Conv2d(512, 4, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
    reg_layer.weight.data.normal_(0, 0.01)
    reg_layer.bias.data.normal_(0, 0.01)
    base_encoder.add_module('reg_layer', reg_layer)
    base_encoder.add_module('relu', nn.ReLU(inplace=True))  # output: [512, 50, 50]
    return base_encoder


def feature_maps(data_collection: dict, image_encoder: nn.Sequential):
    if data_collection['batch_size'] is None:
        img_shape = data_collection['img_shape']
        scale = data_collection['down_sample_scale']
        final_channels = data_collection['final_channels']

        # img_batch = torch.tensor((data_collection['img'].reshape(1, *img_shape)), device='cuda')
        img_batch = torch.tensor(data_collection['img'].reshape(1, *img_shape))
        # image_encoder = image_encoder.double().cuda()
        image_encoder = image_encoder.double()
        featMap = image_encoder(img_batch)
        featMap = featMap.reshape((final_channels, int(img_shape[-2] / scale), int(img_shape[-1] / scale)))

    else:
        img_batch = torch.tensor((data_collection['img']), device='cuda')
        # image_encoder = image_encoder.double().cuda()
        image_encoder = image_encoder.double()
        featMap = image_encoder(img_batch)

    return featMap


if __name__ == '__main__':
    # dataset = Dataset(Path('./line_detection_dataset/').absolute())
    pass
